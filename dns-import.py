#!/usr/bin/python
# -*- coding:utf-8 -*-
import json
import sqlite3
from pprint import pprint

json_files = ["RIPE-Atlas-measurement-4478521.json", # good.ecdsa.cz,RD,DO,CD
              "RIPE-Atlas-measurement-4478524.json", # bad.ecdsa.cz,RD,DO
              "RIPE-Atlas-measurement-4478527.json", # www.rhybar.cz,RD,DO,CD
              "RIPE-Atlas-measurement-4478530.json", # no.ecdsa.cz,RD,DO
              "RIPE-Atlas-measurement-4478522.json", # good.ecdsa.cz,RD,DO,CD
              "RIPE-Atlas-measurement-4478525.json", # www.nic.cz,RD,DO,CD
              "RIPE-Atlas-measurement-4478528.json", # www.rhybar.cz,RD,DO
              "RIPE-Atlas-measurement-4478523.json", # bad.ecdsa.cz,RD,DO,CD
              "RIPE-Atlas-measurement-4478526.json", # www.nic.cz,RD,DO
              "RIPE-Atlas-measurement-4478529.json"] # no.ecdsa.cz,RD,DO,CD

dst_names = { 4478521: 'good.ecdsa.cz',
              4478522: 'good.ecdsa.cz',
              4478523: 'bad.ecdsa.cz',
              4478524: 'bad.ecdsa.cz',
              4478525: 'www.nic.cz',
              4478526: 'www.nic.cz',
              4478527: 'www.rhybar.cz',
              4478528: 'www.rhybar.cz',
              4478529: 'no.ecdsa.cz',
              4478530: 'no.ecdsa.cz'}
              
conn = sqlite3.connect("ecdsa.db")

c = conn.cursor()
c.execute('drop table dns')
c.execute('create table dns (msm_id, prb_id, src_addr, dst_name, flags, dst_addr, ancount, error)')
#c.execute('delete from dns')
#c.execute('create table dns (prb_id, src_addr, dst_name, dst_addr, avg, sent, rcvd)')
#c.execute('delete from dns')

for JSON_FILE in json_files:
    measurement = json.load(open(JSON_FILE))
    count = 0
    for item in measurement:
        src_addr = item["from"]
        prb_id   = item["prb_id"]
        msm_id   = item["msm_id"]
        dst_name = dst_names[msm_id]
        if (msm_id % 2) == 0:
            flags = ""
        else:
            flags = "cd"

        for result in item['resultset']:

            if result.has_key('error'):
                if result['error'].has_key('timeout'):
                    error = 'timeout'
                elif result['error'].has_key('socket'):
                    error = 'socket'
                else:
                    pprint(result)
                    raise Exception("Unknown error %s" % result['error'].keys()[0])
            else:
                error = ""

            if result.has_key('result'):
                dst_addr = result['dst_addr']
                ancount  = result['result']['ANCOUNT']
#                if ancount == 0:
#                    pprint(item)
#                    raise Exception("%s: %s" % (dst_name, cd_flag))
#                    break
            else:
                dst_addr = ""
                ancount  = -1
                
            data = [msm_id, prb_id, src_addr, dst_name, flags, dst_addr, ancount, error]
            c.execute('insert into dns values (?,?,?,?,?,?,?,?);', data)
            count = count + 1
    print "Imported %s entries from %s" % (count, JSON_FILE)

conn.commit()
c.close()
