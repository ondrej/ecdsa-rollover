#!/usr/bin/python
# -*- coding:utf-8 -*-
import json
import sqlite3

json_files = ["RIPE-Atlas-measurement-bad.ecdsa.cz.json",
              "RIPE-Atlas-measurement-good.ecdsa.cz.json",
              "RIPE-Atlas-measurement-no.ecdsa.cz.json",
              "RIPE-Atlas-measurement-www.nic.cz.json",
              "RIPE-Atlas-measurement-www.rhybar.cz.json"]

conn = sqlite3.connect("ecdsa.db")

c = conn.cursor()
c.execute('create table ping (id, prb_id, src_addr, dst_name, dst_addr, avg, sent, rcvd)')

db_id = 0

for JSON_FILE in json_files:
    measurement = json.load(open(JSON_FILE))
    count = 0
    for item in measurement:
        src_addr = item["from"]
        dst_name = item["dst_name"]
        avg = item["avg"]
        prb_id = item["prb_id"]
        rcvd = item["rcvd"]
        sent = item["sent"]
        if item.has_key("dst_addr"):
            dst_addr = item["dst_addr"]
        else:
            dst_addr = ""

        data = [db_id, prb_id, src_addr, dst_name, dst_addr, avg, sent, rcvd]
        c.execute('insert into ping values (?,?,?,?,?,?,?,?);', data)
        count = count + 1
        db_id = db_id + 1

    print "Imported %s entries from %s" % (count, JSON_FILE)
    conn.commit()

c.close()

print "Imported %s entries." % (db_id)
